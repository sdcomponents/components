#/bin/bash
/usr/bin/docker exec -it $(/usr/bin/docker ps | /bin/grep jwilder | /usr/bin/awk {'print $1'}) /usr/local/bin/docker-gen -only-exposed /app/nginx.tmpl /etc/nginx/conf.d/default.conf;
/usr/bin/docker exec -it $(/usr/bin/docker ps | /bin/grep jwilder | /usr/bin/awk {'print $1'}) nginx -s reload;
